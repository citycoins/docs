---
description: Links and information about deployed CityCoin contracts.
---

# Contracts

## CityCoin Protocol

* [citycoin-vrf](https://explorer.stacks.co/txid/0x6e18dbc3a687270a7015ebef73a590d4f066538b41bf7cb7c8d7d891b6442964)\
  single contract used by all cities, takes a given stacks height and returns a uint via the on-chain VRF
* [citycoin-core-trait](https://explorer.stacks.co/txid/0x9751f503896ca13ba797e119d1b62d990b854bee3a63301e737fa9d3ebf8ffa6)\
  single contract used by all cities, defining what a core-v1 contract should have at a minimum
* [citycoin-token-trait](https://explorer.stacks.co/txid/0xf64fedb420622d0403154465c176e06ecbbed306ec2337f9fb8f7bbe6c6a8575)\
  single contract used by all cities, defining what a token contract should have at a minimum

## MiamiCoin (MIA)

* [miamicoin-core-v1](https://explorer.stacks.co/txid/0x224eb853ab072591c382b1c917136dcdd6590df80ab646bfed432d779612258f?chain=mainnet)\
  contract that enables mining, stacking, and related claims
* [miamicoin-token](https://explorer.stacks.co/txid/0xc513b769c261233865c43f101438bd6359636ecacfe34576e6424d6c2629174e?chain=mainnet)\
  contract that enables sending, send-many, and minting of the token
* [miamicoin-auth](https://explorer.stacks.co/txid/0x3c9303ada7f0dbf6f814722cb4a6c13752f187bde0b800bd6f84a342505e006b?chain=mainnet)\
  contract that enables administrative functions
* [Miami Wallet Address](https://explorer.stacks.co/address/SM2MARAVW6BEJCD13YV2RHGYHQWT7TDDNMNRB1MVT?chain=mainnet)\
  address used by the contract for MiamiCoin protocol distribution

## NewYorkCityCoin (NYC)

*   [newyorkcitycoin-core-v1](https://explorer.stacks.co/txid/0x1cc470bc57e8e662055872f2eaeb33ed9aec0a5e46a9f6bf17851447ab1ff84c)

    contract that enables mining, stacking, and related claims
*   [newyorkcitycoin-token](https://explorer.stacks.co/txid/0x9c8ddc44fcfdfc67af5425c4174833fc5814627936d573fe38fc29a46ba746e6)

    contract that enables sending, send-many, and minting/burning of the token
*   [newyorkcitycoin-auth](https://explorer.stacks.co/txid/0x7df54b8594149b20567120f25663885cf798526951b956cc160f2ec908723bb3)

    contract that enables administrative functions
*   [New York City Wallet Address](https://explorer.stacks.co/address/SM18VBF2QYAAHN57Q28E2HSM15F6078JZYZ2FQBCX?chain=mainnet)

    address used by the contract for NewYorkCityCoin protocol distribution
