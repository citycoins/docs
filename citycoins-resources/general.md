---
description: General resources centered around the CityCoins ecosystem.
---

# General

## Community Connections

CityCoins are ultimately powered by the community around them. Connect with fellow CityCoiners!

[Blog](https://citycoins.co/blog) | [Discord](https://discord.gg/citycoins) | [FAQ](https://www.citycoins.co/citycoins-faq) | [Telegram](https://t.me/joinchat/zXQLjBgZzIYxNjI8) | [Twitter](https://twitter.com/minecitycoins) | [Website](https://citycoins.co) | [YouTube](https://www.youtube.com/channel/UCOPzQ6DU6agjOweTNydRtTA)

## Community Tools

{% hint style="info" %}
See something missing or incorrect? Come [join the Discord](https://discord.gg/citycoins) and let us know in the #docs channel!
{% endhint %}

| Link                                                                                                                        | Creator                                                 | Description                                                                                                                                                                             |
| --------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Block Time Calculator](https://stxtime.stxstats.xyz)                                                                       | [FriendsFerdinand](https://twitter.com/FriendsFerdina1) | Calculate an estimated amount of time between Stacks block heights                                                                                                                      |
| [Catamaran Swaps](https://www.catamaranswaps.org)                                                                           | [Friedger](https://friedger.de)                         | Trustless swaps between Bitcoin, Stacks, and SIP-010 tokens ([tutorial here](https://thetutorials.notion.site/thetutorials/How-to-use-Catamaranswaps-c9c0b864bdfc4f01b656be468b15d526)) |
| [cryptoacptd](https://cryptoacptd.com)                                                                                      | [cryptoacptd.btc](https://twitter.com/cryptoacptd)      | Directory of businesses in Miami that accept crypto, including MIA                                                                                                                      |
| [MiamiCoin Analysis Script](https://gitlab.com/riot.ai/clarity-pool-tools/-/blob/master/tool-scripts/analysis-citycoins.ts) | [Friedger](https://friedger.de)                         | A tool to download all transactions for the MiamiCoin core and token contracts                                                                                                          |
| [MiamiCoin Block Explorer](https://miamining.com)                                                                           | [jamil.btc](https://mobile.twitter.com/jamilbtc)        | Block explorer and statistics for MiamiCoin                                                                                                                                             |
| [MiamiVoice](https://miamivoice.org)                                                                                        | [drewfalkman.btc](https://twitter.com/drewfalkman)      | A MiamiCoin app that allows you to propose ideas and vote on how Miami should spend their funds raised through MiamiCoin (MIA) mining                                                   |
| [MineMiamiCoin.com UI](https://github.com/syvita/mmc)                                                                       | [Syvita Guild](https://twitter.com/syvitaguild)         | The repository for the mining interface hosted at [minemiamicoin.com](https://minemiamicoin.com)                                                                                        |
| [Mining Calculator](https://docs.google.com/spreadsheets/d/1pR9q6MAFrPjXoDNjQFMOZW6MQE1piTsXausYQyABWqk/edit#gid=0)         | [benoror.btc](https://twitter.com/benoror)              | Google spreadsheet with probability calculations based on block commits                                                                                                                 |
| [NewYorkCityCoin Block Explorer](https://mining.nyc)                                                                        | [jamil.btc](https://mobile.twitter.com/jamilbtc)        | Block explorer and statistics for NewYorkCityCoin                                                                                                                                       |
| [NewYorkCityCoin Mining Tutorial](https://thetutorials.notion.site/How-to-mine-NYC-727a74c8d8964d1aa7d110ff19929272)        | `@pneppl`                                               | A mining tutorial hosted as a Notion site                                                                                                                                               |
| [StacksOnChain: Whales](https://stacksonchain.com/tokenwhales)                                                              | [stacksonchain.btc](https://twitter.com/anononchain)    | Dashboard showing the top ten largest wallet balances by Stacks address                                                                                                                 |
| [Syvita Mining](https://syvitamining.com)                                                                                   | [Syvita Guild](https://twitter.com/syvitaguild)         | Trustless mining pool implementation for CityCoins with mining and Stacking features.                                                                                                   |
| [WenBlok](https://foragerr.github.io/wenblok/)                                                                              | [RaGe.btc](https://twitter.com/fora9err)                | A combination of blocks and estimated dates for important milestones in CityCoins                                                                                                       |

## Brand Resources

CityCoins brand assets are available on the CDN hosted through CloudFlare Pages.

Anything found in [this GitHub repo](https://github.com/citycoins/cdn) can also be found at [https://cdn.citycoins.co](https://cdn.citycoins.co).

{% hint style="info" %}
For example, the main CityCoins brand guide located at the link below:

[https://github.com/citycoins/cdn/blob/main/cdn/brand/CityCoins\_BrandGuidelines.pdf](https://github.com/citycoins/cdn/blob/main/cdn/brand/CityCoins\_BrandGuidelines.pdf)



Is also available at:

[https://cdn.citycoins.co/brand/CityCoins\_BrandGuidelines.pdf](https://cdn.citycoins.co/brand/CityCoins\_BrandGuidelines.pdf)
{% endhint %}

The available resources include branding guidelines and assets, hosted logos, and token metadata for each CityCoin.

